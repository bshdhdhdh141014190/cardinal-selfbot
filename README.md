# 🤖 Cardinal-Selfbot
> Cardinal-Selfbot is a Cardinal fork thats made for selfbot use!
## Requirements

1. Discord Bot Token **[Guide](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot)**
2. Node.js v12.0.0 or newer

## 🚀 Getting Started

```
git clone https://gitlab.com/kreato/cardinal-selfbot.git
cd cardinal-selfbot
npm i
```

After installation finishes you can use `node bot.js` to start the bot.
⚠️**Note:You need to enable Presence Intent for it to work**⚠️

## ⚙️ Configuration

then open `settings.json` and fill out the values:
⚠️**Note: Never commit or share your token or api keys publicly** ⚠️
Settings.json
```json
 {"token":"INSERT TOKEN HERE","client":"INSERT BOT CLIENT ID ","tag":".","case":"false","separator":"\\s+","modules":{"discord.js":["",true],"fstorm":["",true],"jimp":["latest",true],"libsodium-wrappers":["",true],"opusscript":["latest",true],"ytdl-core":["",true],"alexa-bot-api":["",true],"scrape-yt":["",true],"speedtest-net":["latest",true],"simple-youtube-api":["",true],"node-cron":["latest",true],"ytsr":["latest",true],"node-opus":["",true],"@discordjs/opus":["latest",true],"request":["",true],"ffmpeg-static":["latest",true],"google-translate-api":["",true],"ytdl":["",true],"google-tts-api":["latest",true],"node-fetch":["",true],"chalk":["",true],"ejs":["",true]},"ownerId":"OWNER ID","Bot Intents":{"customData":{"Bot Intents":{"intents":32511}}},"DBM Dashboard":{"customData":{"DBM Dashboard":{"port":"3000","clientSecret":"CLIENT SECRET","callbackURL":"http://localhost:3000/dashboard/callback","owner":"OWNER ID","supportServer":"https://discord.gg/"}}},"Bot Dashboard":{"customData":{"Bot Dashboard":{"port":"3000","clientSecret":"","callbackURL":"http://localhost:3000/dashboard/callback","owner":"OWNER ID","supportServer":"https://discord.gg/gay"}}}} 
```

## 📝 Features & Commands

> Note: The default prefix is '--'
> it has;
*Giveaway system
*Moderation features
*fun commands
*and more!


## 🤝 Contributing

1. [Fork the repository](https://gitlab.com/kreato/cardinal-selfbot/-/forks/new)
2. Clone your fork: `git clone https://gitlab.com/your-username/cardinal-selfbot.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -am 'Add some feature'`
5. Push to the branch: `git push origin my-new-feature`
6. Submit a pull request

## 📝 Credits

[@dbm-network](https://github.com/dbm-network) for the DBM Mods that made this bot awesome [@dbm-network/mods](https://github.com/dbm-network/mods)
